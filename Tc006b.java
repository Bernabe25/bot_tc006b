package io.testproject.generated.tests.testproject20213;

import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.testproject.sdk.drivers.ReportingDriver;
import io.testproject.sdk.drivers.android.AndroidDriver;
import io.testproject.sdk.interfaces.junit5.ExceptionsReporter;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * This class was automatically generated by TestProject
 * Project: Test Project 2021-3
 * Test: TC006B
 * Generated by: Bernabé Garcia (bernabegarcia284@gmail.com)
 * Generated on Tue Oct 12 17:18:42 GMT 2021.
 */
@DisplayName("TC006B")
public class Tc006b implements ExceptionsReporter {
  public static AndroidDriver<? extends MobileElement> driver;

  @BeforeAll
  static void setup() throws Exception {
    driver = new AndroidDriver<>("8iXkJZMdo12B7zlreJ1BCF5UUyV_d9dRhWjVzMvbFw4", getCapabilities(), "Bernabé Garcia");
  }

  /**
   * In order to upload the test to TestProject need to un-comment @ArgumentsSource and set in comment the @MethodSource
   * @org.junit.jupiter.params.provider.ArgumentsSource(io.testproject.sdk.interfaces.parameterization.TestProjectParameterizer.class) */
  @DisplayName("TC006B")
  @ParameterizedTest
  @MethodSource("provideParameters")
  void execute(String Opcion, String Ciudad, String Incorrecto) throws Exception {
    // set timeout for driver actions (similar to step timeout)
    driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
    By by;
    boolean booleanResult;

    // 1. Click 'ANDROID.WIDGET.FRAMELAYOUT'
    GeneratedUtils.sleep(2000);
    by = By.xpath("//android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout");
    driver.findElement(by).click();

    // 2. Click 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).click();

    // 3. Type '{{Opcion}}' in 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).sendKeys(Opcion);

    // 4. Click 'Enviar'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/send");
    driver.findElement(by).click();

    // 5. Click 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).click();

    // 6. Type '{{Ciudad}}' in 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).sendKeys(Ciudad);

    // 7. Click 'Enviar'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/send");
    driver.findElement(by).click();

    // 8. Click 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).click();

    // 9. Type '{{Incorrecto}}' in 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).sendKeys(Incorrecto);

    // 10. Click 'Enviar'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/send");
    driver.findElement(by).click();

    // 11. Click 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).click();

    // 12. Type '0' in 'Mensaje'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/entry");
    driver.findElement(by).sendKeys("0");

    // 13. Click 'Enviar'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/send");
    driver.findElement(by).click();

    // 14. Click 'com.whatsapp:id/whatsapp_toolbar_home'
    GeneratedUtils.sleep(2000);
    by = By.id("com.whatsapp:id/whatsapp_toolbar_home");
    driver.findElement(by).click();

  }

  @Override
  public ReportingDriver getDriver() {
    return (ReportingDriver) driver;
  }

  @AfterAll
  static void tearDown() {
    if (driver != null) {
      driver.quit();
    }
  }

  private static Stream provideParameters() throws Exception {
    return Stream.of(Arguments.of("1","1","5"));
  }

  public static DesiredCapabilities getCapabilities() {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
    capabilities.setCapability(MobileCapabilityType.UDID, "WUJ01PV8DJ");
    capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
    capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.whatsapp");
    capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.whatsapp.Main");
    return capabilities;
  }
}
